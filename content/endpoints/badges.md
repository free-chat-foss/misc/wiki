---
title: Badges
anchor: badges-ep
weight: 19
---

### `/badge/new`

It should be noted that while alpha values are technically valid it is up to 
individual clients to decide what to do with that value. Personally I suggest not
doing anything with the alpha value so that it doesn't matter but that is up to 
the client developer.

* Required query string parameters:

	* badge\_name: String
		* Name to assign to the new badge
	* badge\_perms: u64
		* Permissions mask which apply to the badge

* Semi optional query string parameter:

	* badge\_color: u32
		* If not included the color will default to `0xffffffff`

* Required permissions

	* NEW\_BADGE

* Returns
	* A new [badge](#badges) behind the JSON key `badge`.


### `/badge/delete`

* Required query string parameters:

	* id: u64
		* ID of the target badge to delete

* Required permissions

	* DELETE\_BADGE

* Returns

	* HTTP 200 on success

Example:

```
	> POST /badge/new

	< {
	< 	"badge": {
	<		"id": 1,
	<		"name": "badge-name-here",
	<		"color": 1234,
	<		"perms": 1234,
	<	}
	< }
```

### `/badge/list`

* No query string required.

* No required permissions, only that the client have a valid JWT.

* Returns 
	* Array of [Badges](#badges) behind the JSON key `badges`.

Example:

```
	> GET /badge/list

	< {
	< 	"badges": [
	< 		{
	<			"id": 1,
	<			"name": "badge-name-here",
	<			"color": 1234,
	<			"perms": 1234,
	<		},
	<		...
	<	]
	< }
```

### `/badge/update/color`

* Required query string parameters:

	* badge\_id: u64
		* ID of the target badge to update.

	* badge\_color: u32
		* The new color to update to

* Required permissions:

	* UPDATE\_COLOR\_BADGE

* Returns:

	* HTTP 200

### `/badge/update/name`

* Required query string parameters:

	* badge\_id: u64
		* ID of the target badge toupdate.

	* badge\_name: String
		* The new badge name to assign.

* Required permissions:

	* UPDATE\_NAME\_BADGE

### `/badge/update/perms`

* Required query string parameters:

	* badge\_id: u64
		* ID of the target badge toupdate.

	* badge\_perms: u64
		* Represented as a regular integer this is the bitmap of permissions which apply to the badge

* Required permissions:

	* UPDATE\_PERMS\_BADGE
