---
title: Invites
anchor: invites-ep
weight: 16
---

### `POST /invite/create`

* Required permissions: 
	* CREATE_TMP_INVITES
		* Base requirement
	* CREATE_PERM_INVITES
		* Additional requirement for permanent invite codes

* Required query string parameters:
	* id: u64
	* jwt: string

* Returns
	* id: i64
	* uses: Optional<i64>
	This field is really meant for servers and really doesn't have much use to users. It just to keep track of whether or not an invite has been used too many times or not.

	* expires: boolean


Example
```
	> POST /invite/create?id=123&jwt=

	< { 
	<	"id": <unix-timestamp ms>, 
	<	"uses": 3, 
	<	"expires": true
	< }
```

### `GET /join`

* Required Permissions
	* None

* Required query string parameters:
	* code: i64

* Returns a new member account - The account details are not behind a key in the JSON object, so the resulting data is completely flat, for JSON.
	* id: u64
	* secret: String
	* name: String
	* status: i32
	* permissions: u64




	
* Default values
	* status: 0
	* permissions: 51
	* name: Anonymous


Example
```
	> GET /join?code=123456

	< { 
	<	"id": 123,
	<	"secret": "super secret",
	<	"name": "Anonymous",
	<	"status": 0,
	<	"permissions": 51
	< } 
```

