---
title: Neighbors
anchor: neighbors-ep
weight: 18
---

### `GET /neighbor/list`

* Required query string parameters 

	* jwt: String

* Returns
	* neighbors: Array\<Neighbor\>


Example
```
	> GET /neighbor/list?jwt=...

	< {
	< 	"neighbors": [
	< 		{
	<			"name": "Instance Name",
	<			"url": "https://instance.xyz",
	<			"wsurl": "wss://instance.xyz",
	<			"description": "Interesting description",
	<			"tags": ["tech", "code", "docs"]
	<		},
	<		...
	<	]
	< }
```


### `POST /neighbor/add`

* Required query string parameters 

	* jwt: String

* Required HTTP Headers
	* content-type: Must be of `application/json`
	* content-length
* Required Body
	* Neighbor structure data(json encoded)

* Returns
	* On sucess: 200 OK

Example
```
	> POST /neighbor/add
	> content-type: application/json
	> content-length: ...
	> {
	>	"name": "Instance Name",
	>	"url": "https://instance.xyz",
	>	"wsurl": "wss://instance.xyz",
	>	"description": "Interesting description",
	>	"tags": ["tech", "code", "docs"]
	> }

	< 200 OK
```



### `PUT /neighbor/update`

This route requires the full neighbor structure in the body with all the fields modified to satisfaction.
The url parameter in the query string is used to uniquely identify a listed neighbor.
_Note that the url can also be changed but the original/previous url must be used to identify
what is currently listed_.


* Required query string parameters 
	* jwt: String
	* url: String := target url of the neighbor to update

* Required headers
	* content-type: application/json
	* content-length

Example
```
	> PUT /neighbor/update
	> content-type: application/json
	> content-length: ...
	> {
	>	"name": "New Instance Name",
	>	"url": "https://new.instance.xyz",
	>	"wsurl": "wss://new.instance.xyz",
	>	"description": "New Interesting description",
	>	"tags": ["new", "tags", "tech", "code", "docs"]
	> }

	< 200 OK
```

### `DELETE /neighbor/delete`

As long as the permissions are correct this route will basically always 200.
Even if nothing is found the endpoint will still 200.

* Required query string parameters 
	* jwt: String
	* url: String

Example
```
	> PUT /neighbor/update?url=https%3A%2F%2Finstance.xyz

	< 200 OK
```
