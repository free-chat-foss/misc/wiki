---
title: Permissions
anchor: permissions
weight: 12
---

Due to the nature of many endpoints,  some endpoints require a user to have certain permissions setup.

User permissions are stored as an `unsigned 64-bit` bit mask alongside other user data. So far the current implemented permissions are listed below as follows.

NOTE: due to the rapid changing of features this list is subject to change until
the protocol reaches `1.0` status.

| Name | Value | Short Description
:------|:-------------:|:----
 JOIN_VOICE| 0x01 | Ability to join a voice channel via 
 SEND_MESSAGES| 0x02 | Ability to send text messages in any text channel
 CREATE_TMP_INVITES| 0x04 | Creation of invite codes which expire
 CREATE_PERM_INVITES| 0x08 | Create of invite codes which do not expire
 CHANGE_NICK | 0x10 | Can change nickname
 ALLOW_PFP|0x20 | Allowed to upload their own profile picture to the server
 CREATE_CHANNEL| 0x40 | Can create channels
 DELETE_CHANNEL| 0x80 | Can delete channels
 ADMIN| 0x4000000000000000 | Can modify any permission beneath them, but not other admins
 OWNER| 0x8000000000000000 | Can modify anything

Typically Admins and Owners will have every role beneath them alongside their own
admin/owner flag for the sake of implementation simplicity although this is of 
no importance to those wanting to write their own client software.



