---
title: Connections
anchor: conns
weight: 7
---


Most servers will likely run on one of three ports, `80` for basic HTTP, `443` for secure HTTP traffic, or `4536` for most others.
If using a _special port_ then servers should expect _user applications_ to specify the port.

For connections over SSH or other protocols it really is up to server owners to let new users know about this important detail. 
SSH based servers are not discouraged, in fact they are highly _encouraged_ to exist and operate :^)

