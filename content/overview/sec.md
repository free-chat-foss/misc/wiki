---
title: Security
anchor: sec-overview
weight: 8
---

**DO NOT STORE PLAINTEXT SECRETS**. For developers that aren't _rarted_ this 
isn't hard but if you think about doing this, don't.

With the secret value compromised there is nothing you can do(that I encourage anyways)
to tell if that account has been stolen.
If for some reason you(an admin) believes an account has been stolen, then
lock the account by removing its permissions.

JSON Web Tokens are used as a kind of session/cookie system as user applications
are not assumed to be browsers ever.
Freechat servers do not assume user apps to be browser to give people maximum
freedom in both application choice and developers freedom to use whatever hipster
libraries/frameworks languages they want.

* JWT's should **never** last more than 1 week
	* 24 hours is fine but they should **always expire**

* JWT's should **always** be re assignable with the /login route

>I hate jwt

Sucks that what we use until a more flexible standard is designed/developed/doc'd.

