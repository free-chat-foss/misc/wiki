---
title: User Applications
anchor: user-overview
weight: 9
---

As long as the application is ready to deal with receiving data in the forms
specified in [the structures section](#structures).

App developers should keep in mind that data received is nearly always JSON.
The only exception is with empty body responses.

For this reason there is really two things to look for on server response:

1. HTTP Status Code

2. `Content-Type` header which if present is `application/json`; signifying
the body contains some JSON data.

It's safe to assume that if this isn't present the HTTP body should be ignored.

## Authorizing

To authorize on most routes you need to put the following in the query string:

* `id` - The user id for that server

* `jwt` - JSON web token
	* It's suggested that this be refreshed when you application starts to not
	get hit with random HTTP:400's on accident

To get a valid JSON web token to use check the `/login` route details under the 
[authorization section](#auth-ep).

## Making requests

Most request data goes in the query string however, large payloads are the exception.
For example the `/messages/send` expects the content of the message to be 
sent in the request body while the query string is used to describe what the
request body is supposed be.

* Request Headers

	Are not required since Freechat servers assume the worst of clients 
so they don't bother ever looking there.
	Save yourself the bandwidth and don't put anything there(or do idc tbh).

* Response Headers
	
	As of now the only thing to look for is a present `Content-Type: application/json`.

