---
title: WebRTC
anchor: webrtc
weight: 40
---

## Configuration

By default web socket servers are bound to port `5648`. 
The public URL for this server is also defined in the `.env` configuration file under 
the key value of `PUBLIC_WS_URL`.

Due to how `events` are propagated to users the web socket server also requires
a special `wss-hmac.secret` file to be in the same directory as the required
`hmac.secret` file.
This HMAC is what is used to discern server connections from user connections

## Data structure

Event messages come serialized in JSON form.
They have the structure of:

```
{
	"type": <event-type>,
	<event-type>: <data-structure>
}
```

The full list of supported events are as follows:

* new-message
* delete-channel
* create-channel
* update-nick

