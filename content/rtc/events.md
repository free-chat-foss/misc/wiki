---
title: Events
anchor: events
weight: 43
---
## Pertinent Event Data Structures

This section describes the data you probably care about; the data behind `<data-type>`.

### `new-message`

Each valid `new-message` is completely flat and contains the following fields:

* id: u64
Public user id
* time: i64

Unix time-stamp (generated on the server's end)

* content: String

If the content type is `text/plain` then the content _is_ all there is for that message.
If the content type is another _valid_ type then this field will contain null.
A second `/message/get?id=<id>` is required. This is done to avoid forcing tons of file data across a network when you don't want it. 

This also gives client devs the choice of putting that network hit behind a button(or some user action) or doing automatically.


* content\_type: String

_For valid content-types refer to [/message/send documentation](#messages-ep)_

* author\_id: u64

Public Member id

* channel\_id: u64

Text channel id. It is safe to assume that this field _will_ point to a valid text channel and not a misconfigured voice channel.

* name: String

Public username for that given user


### `delete-channel`

Contains only 1 field:

* id: u64

The id of the deleted channel

### `create-channel`

* id: u64

The id of the new channel

* name: String

Name of channel

* description: String | null

Optional description of channel 

* kind: i32

Recall:

* Voice channels := 1
* Text channels : 2

### `update-nick`

* id: u64

Id of affected user

* name: String

New nickname of user

### `new-badge`

* id: u64

*  name: String

* color: u32

* perms: u64

_Notes regarding data structure:_ 

* None of these fields are optional so if they are not all present the incoming data should be ignored.


### `update-badge-name`

* id: u64

* name: String

New name for of the badge referenced by `id`.

### `update-badge-perms`

* id: u64

* perms: u64

New permissions mask of the badge referenced by `id`.

### `update-badge-color`

* id: u64

* color: u32

New color for the badge referenced by `id`.

### `delete-badge`

* id: u64

Badge which has been deleted
