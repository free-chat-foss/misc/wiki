---
title: Event Structure
anchor: event-structure
weight: 42
---

There are a few rules regarding RTC event data structures:

1. They are always JSON encoded

2. Top level keys are for either meta data or a root node which leads to actual event data


Top level fields are as follows:

* type: string

Denotes the event type as a string. This value is used as a key to identify the
actual event data itself.

* \<type\>: \<data\>

The key here is found by checking the value associated with "type". 
Behind this event data can be found; for example a `new-message` event payload
would look like:

```
{
	"type": "new-message",
	"new-message": {
		"id": 123,
		"time: 1623730090
	}
}
```


