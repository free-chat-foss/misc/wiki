---
title: Badges
anchor: badges
weight: 55
---


  Name  |   Type
:--------:|:-------:
 `name`   | `String`
 `id`     | `u64`
 `color`  | `u32`
 `perms`  | `u64`

