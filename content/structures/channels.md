---
title: Channels
anchor: channels
weight: 35
---


Channels are made up of the following components

 Name | Type 
:------:|:--------:
 id   | `u64`
 name | `String`
 kind | `i32`
description| `String`
badge\_ids| `Array<u32>`


Channels have two valid types or (more semantically) `kind` values: 

* Voice Channel = 1

* Text Channel = 2

