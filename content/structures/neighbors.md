---
title: Neighbors
anchor: neighbors-structure
weight: 45
---

Neighbors contain the following structure

Name | Type
:-----:|:-----:
  url\*  | String
 wsurl | String
 name  | String
description | String
tags | Array\<String\>


* url: is used as the primary key for identifying neighbors from one another
