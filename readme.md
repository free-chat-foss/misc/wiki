 # Freechat Wiki Source Code

 This directory basically contains all the required code to build the wiki page
 which is found at https://freechat.shockrah.xyz

## Printing/PDF Format

While I don't provide a pdf format of the documentation at the moment the main 
API reference is somewhat printer friendly.
If anyone wants to provide a script to provide the website in other formats 
it would be greatly appreciated.
